/*SubAtlas creates Parte which is each object which has the folowing URLs:
* original: NRRD
* segmentado: NRRD
* miniatura: PNG
* etiquetas: txt
* title: string*/

import Parte from "./Parte";
import {getParameterByName} from "../constantRoutes";

export default class SubAtlas {
    constructor(subAtlas) {
        this.subAtlas = subAtlas.map((urlsLoadedFromJSON) => {
            const newParte = new Parte();
            newParte.createSegmentos(urlsLoadedFromJSON);
            newParte.createMiniatura(urlsLoadedFromJSON);
            newParte.createNrrdVolumen(urlsLoadedFromJSON);
            newParte.setTitle(urlsLoadedFromJSON.titulo);
            return newParte;
        });
    }

    getParteActual(index = 0) {
        return this.subAtlas[getParameterByName('parte') || index];
    }

    getSubAtlas() {
        return this.subAtlas;
    }
}
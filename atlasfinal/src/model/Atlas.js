/*Atlas is an array which holds all the SubAtlas*/
import {getParameterByName} from "../constantRoutes";
import SubAtlas from "./SubAtlas";

export default class Atlas {
    constructor() {

        this.atlas = [];

        this.lastSegmentClicked = null;
        this.currentSegmentClicked = null;
        this.indexesOfClickedSegments = [];
    }

    getIndexesOfClickedSegments() {
        return this.indexesOfClickedSegments;
    }

    addSubAtlas(subAtlas) {
        this.atlas.push(new SubAtlas(subAtlas));
    }

    getSubAtlasActual(index = 0) {
        return this.atlas[getParameterByName('subAtlas') || index];
    }

    getLastSegmentClicked() {
        return this.lastSegmentClicked;
    }

    getCurrentSegmentClicked() {
        return this.currentSegmentClicked;
    }

    setLastSegmentClicked(index) {
        this.lastSegmentClicked = index;
    }

    setCurrentSegmentClicked(index) {
        this.currentSegmentClicked = index;
    }

}


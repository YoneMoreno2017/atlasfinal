/*Segmento reads a txt file and creates an object where there is name and colors*/

export default class Segmento {
    constructor(etiquetas) {
        this.etiquetas = etiquetas;
        this.text = '';
        this.names = '';
        this.colors = '';
    }

    readTextFile = (url) => {
        const rawFile = new XMLHttpRequest();
        rawFile.open("GET", url, false);
        rawFile.overrideMimeType('text/xml; charset=iso-8859-1');
        rawFile.onreadystatechange = () => {
            if (rawFile.readyState === 4) {
                this.text = rawFile.responseText;
            }
        };
        rawFile.send();
    };

    splitLines(text) {
        return text.split('\n');
    }

    splitWords(line) {
        return line.split('" "').map((word) => word.replace('"', ''));
    }

    createNames(text) {
        const lines = this.splitLines(text);
        this.names = this.splitWords(lines[0]);
    }

    createColors(text) {
        const lines = this.splitLines(text);
        this.colors = this.splitWords(lines[1]);
    }

    createAllSegmentos() {
        this.segmentos = this.names.map((name, index) => {
            return {
                name: name,
                color: this.colors[index],
                indice: index
            }
        });
    }
}
/*Parte has the URLs loaded from the JSON which points to NRRD, PNG and txt.
* It has an array of segmento which are the elements with name and color of each Parte*/
import Segmento from "./Segmento";
import Miniatura from "./Miniatura";
import NrrdVolumen from "./NrrdVolumen";
import {LANGUAGE_MODE} from "../constantRoutes";

export default class Parte {
    constructor() {
        this.segmento = null;
        this.miniatura = null;
        this.nrrdVolumen = null;
        this.title = '';
    }


    createSegmentos(urlsLoadedFromJSON) {
        this.segmento = new Segmento(urlsLoadedFromJSON[localStorage.getItem(LANGUAGE_MODE) || 'etiquetas']);
        this.segmento.readTextFile(this.segmento.etiquetas);
        this.segmento.createColors(this.segmento.text);
        this.segmento.createNames(this.segmento.text);
        this.segmento.createAllSegmentos();
    }

    createMiniatura(urlsLoadedFromJSON) {
        this.miniatura = new Miniatura(urlsLoadedFromJSON.miniatura);
    }

    createNrrdVolumen(urlsLoadedFromJSON) {
        this.nrrdVolumen = new NrrdVolumen(urlsLoadedFromJSON.original,
            urlsLoadedFromJSON.segmentado);
    }

    getSegmentos() {
        return this.segmento.segmentos;
    }

    getMiniatura() {
        return this.miniatura.miniatura;
    }

    getNrrdVolumen() {
        return this.nrrdVolumen;
    }

    setTitle(title) {
        this.title = title;
    }

    getTitle() {
        return this.title;
    }


}

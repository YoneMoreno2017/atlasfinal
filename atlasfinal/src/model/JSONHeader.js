/*JSONHeader loads all the files in the folder specified as location*/
import jQuery from 'jquery-ajax';
import Atlas from "./Atlas";


export default class JSONHeader {
    constructor(location, callback) {
        loadAtlasStructure(location, callback);

        function loadAtlasStructure(location, callback) {
            jQuery.ajax({
                    dataType: "json",
                    url: location,
                    async: true,
                    success: function (files) {
                        files.map((file, index, allFilesArray) => {
                            jQuery.ajax({
                                dataType: "json",
                                url: location + file,
                                async: true,
                                success: function (data) {
                                    if (!window.IndexAtlas) {
                                        window.IndexAtlas = new Atlas();
                                    }
                                    window.IndexAtlas.addSubAtlas(data);
                                    if (allSubAtlasHaveBeenLoaded(index, allFilesArray)) {
                                        callback();
                                    }
                                }
                            });
                        })
                    }
                }
            )
        }
    }
}

function allSubAtlasHaveBeenLoaded(index, allFilesArray) {
    return index >= allFilesArray.length - 1;
}
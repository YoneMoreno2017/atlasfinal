export default class NrrdVolumen {
    constructor(original, segmentado) {
        this.original = original;
        this.segmentado = segmentado;
    }

    getOriginal() {
        return this.original;
    }

    getSegmentado() {
        return this.segmentado;
    }
}
/*CoverPage shows a canvas with the horse. Credits, and buttons to change the language and
* study mode*/

import React, {Component} from 'react';
import './styles.css';
import SplitterLayout from 'react-splitter-layout';
import Header from "../Header/index";
import LanguagesModes from "../LanguageModes/index";
import StudyModes from "../StudyModes/index";
import portada from './portada.png';
import AtlasButtons from '../AtlasButtons';

class CoverPage extends Component {

    render() {
        return (
            <div>
                <Header leftTitle='ULPGC: Osteología del caballo'/>
                <SplitterLayout
                    vertical={true}
                    percentage={true}
                    secondaryInitialSize={25}
                >
                    <div className='text-center'>
                        <img id='CoverPageImage' src={portada}>
                        </img>
                    </div>
                    <div>
                        <SplitterLayout
                            vertical={false}
                            percentage={true}
                            secondaryInitialSize={83}
                            secondaryMinSize={83}
                            primaryMinSize={17}
                        >
                            <StudyModes/>
                            <SplitterLayout
                                vertical={false}
                                percentage={true}
                                secondaryInitialSize={75}
                                secondaryMinSize={75}
                                primaryMinSize={25}
                            >
                                <LanguagesModes/>
                                <AtlasButtons/>
                            </SplitterLayout>
                        </SplitterLayout>
                    </div>
                </SplitterLayout>
            </div>
        );

    }
}

export default CoverPage;
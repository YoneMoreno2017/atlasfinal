/*It is responsible for get all Segmentos and display their name.*/

import React, {Component} from 'react';
import './styles.css';
import StudyMode from "../threejs/StudyMode";
import {STUDY, STUDY_MODE} from "../../constantRoutes";


export default class SegmentsList extends Component {
    constructor(props) {
        super(props);
        this.state = {segmentos: [], activeWord: -1, rebuild: false};

        this.studyMode = new StudyMode();

        window.segmentsListState = this.state;
        window.onSegmentListItemClickFunction = this.onClickFunction;

        window.rebuildSegmentsList = this.rebuildSegmentsList.bind(this);
    }

    rebuildSegmentsList() {
        this.setState({rebuild: true, activeWord: -1});

        if (window.partIndex != null) {

            this.setState(() => ({
                segmentos: this.props.IndexAtlas.getSubAtlasActual().subAtlas[window.partIndex]
                    .getSegmentos().filter((segmento) =>
                        segmento.name.length > 0)
                    .sort((function sortAlphabetically(segmentoA, segmentoB) {
                        if (segmentoA.name > segmentoB.name) {
                            return 1;
                        }
                        if (segmentoA.name < segmentoB.name) {
                            return -1;
                        }
                        return 0;
                    }))
            }));

            const segmentsListItems = Array.from(document.getElementsByClassName('segmentsList'));

            segmentsListItems.map((listItem) => listItem.style.backgroundColor = '');
        }
    }

    onClickFunction = (idx) => {
        this.setState({activeWord: idx});


        this.studyMode.applyStudyMode(window.multiVolumeSlice.getLabelSlice(), idx);
    };

    componentDidUpdate() {
        const selectedSegmentListItem = document.getElementsByClassName('selected')[0];


        if (selectedSegmentListItem) {
            selectedSegmentListItem.style.backgroundColor =
                window.multiVolumeSlice.getLabelSlice().arrayColors[this.state.activeWord];
        }
    }

    componentWillUpdate() {

        const selectedSegmentListItem = document.getElementsByClassName('selected')[0];

        if (selectedSegmentListItem && localStorage.getItem(STUDY_MODE) === STUDY) {
            selectedSegmentListItem.style.backgroundColor = 'lightgray';
        }
    }


    componentDidMount() {


        this.setState(() => ({
            segmentos: this.props.IndexAtlas.getSubAtlasActual().getParteActual()
                .getSegmentos().filter((segmento) =>
                    segmento.name.length > 0)
                .sort((function sortAlphabetically(segmentoA, segmentoB) {
                    if (segmentoA.name > segmentoB.name) {
                        return 1;
                    }
                    if (segmentoA.name < segmentoB.name) {
                        return -1;
                    }
                    return 0;
                }))
        }));


    }

    render() {
        return (
            <div>
                <ul style={{marginBottom: '50px'}}>
                    {this.state.segmentos.map((segmento) =>
                        <li
                            className={`segmentsList${this.state.activeWord === segmento.indice ? ' selected' : ''}`}
                            onClick={this.onClickFunction.bind(null, segmento.indice)}
                            key={segmento.indice}
                        >
                            {segmento.name}
                        </li>
                    )}
                </ul>
            </div>

        );
    }
}




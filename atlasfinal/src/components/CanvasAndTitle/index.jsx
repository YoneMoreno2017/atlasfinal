/*TwoNrrdCanvas display two canvas horizontally*/

import React, {Component} from 'react';
import SplitterLayout from 'react-splitter-layout';
import NrrdCanvas from "../NrrdCanvas/index";
import './styles.css';
import Title from "../Title/index";


class CanvasAndTitle extends Component {
    render() {
        return (
            <SplitterLayout
                vertical={true}
                percentage={true}
                primaryMinSize={85}
                secondaryInitialSize={15}
                secondaryMinSize={15}
                style={{
                    backgroundColor: 'black'
                }}
            >
                <NrrdCanvas
                    width='80vw'
                    height='82vh'
                    IndexAtlas={this.props.IndexAtlas}
                />
                <Title id='partTitle' title={this.props.IndexAtlas.getSubAtlasActual() &&
                this.props.IndexAtlas.getSubAtlasActual().getParteActual().getTitle()}/>
            </SplitterLayout>
        );
    }
}

export default CanvasAndTitle;


/*It has the buttons to change the language*/
import React, {Component} from 'react';
import {Button, ButtonGroup} from 'reactstrap';
import Alert from "../Alert/index";
import {ENGLISH, LANGUAGE_MODE, SPANISH} from "../../constantRoutes";
import StudyModes from "../StudyModes";


class LanguageModes extends Component {

    constructor() {
        super();

        this.state = ({showAlert: false, title: '', inEnglish: false})
    }

    componentDidUpdate(nextProps, nextState) {

        if (!nextState.showAlert) {
            this.turnOffAlert = setTimeout(() => {
                this.setState(() => ({showAlert: false}))
            }, 3000);
        }
    }

    changeLanguageModesTextToEnglish() {
        this.setState(() => ({inEnglish: true}));
    }

    changeLanguageModesTextToSpanish() {
        this.setState(() => ({inEnglish: false}));
    }

    componentWillUnmount() {
        clearTimeout(this.turnOffAlert);
    }


    render() {
        let english = this.state.inEnglish;
        return (
            <div>
                {english || localStorage.getItem(LANGUAGE_MODE) === ENGLISH ? (
                        <div>
                            <h6>Language:</h6>

                            <div className="row">
                                <div className="col-6">
                                    <ButtonGroup>
                                        <Button
                                            onClick={() => {
                                                localStorage.setItem(LANGUAGE_MODE, SPANISH);
                                                this.setState({showAlert: true, title: `Spanish`});
                                                this.changeLanguageModesTextToSpanish();
                                                window.changeTextToSpanish();
                                            }}
                                            className='ml-3 mr-3 btn-sm' color="primary"> Spanish
                                        </Button>
                                        <Button
                                            onClick={() => {
                                                localStorage.setItem(LANGUAGE_MODE, ENGLISH);
                                                this.setState({showAlert: true, title: `English`});
                                                this.changeLanguageModesTextToEnglish();
                                                window.changeTextToEnglish();
                                            }}
                                            className='ml-3 mr-3  btn-sm' color="primary">English</Button>


                                    </ButtonGroup>
                                </div>
                            </div>
                        </div>

                    ) :
                    (
                        <div>
                            <h6>Idioma:</h6>

                            <div className="row">
                                <div className="col-6">
                                    <ButtonGroup>
                                        <Button
                                            onClick={() => {
                                                localStorage.setItem(LANGUAGE_MODE, SPANISH);
                                                this.setState({showAlert: true, title: `Español`});
                                                this.changeLanguageModesTextToSpanish();
                                                window.changeTextToSpanish();
                                            }}
                                            className='ml-3 mr-3 btn-sm' color="primary"> Español
                                        </Button>
                                        <Button
                                            onClick={() => {
                                                localStorage.setItem(LANGUAGE_MODE, ENGLISH);
                                                this.setState({showAlert: true, title: `Inglés`});
                                                this.changeLanguageModesTextToEnglish();
                                                window.changeTextToEnglish();
                                            }}
                                            className='ml-3 mr-3  btn-sm' color="primary">Inglés</Button>


                                    </ButtonGroup>
                                </div>
                            </div>
                        </div>
                    )
                }
                <div className="row">
                    <div className="col-12">
                        {
                            this.state.showAlert &&
                            <Alert
                                type='info'
                                show={this.state.showAlert}
                                title={this.state.title}
                            />
                        }
                    </div>
                </div>
            </div>
        );
    }
}


export default LanguageModes;
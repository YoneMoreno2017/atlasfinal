import React, {Component} from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button
} from 'reactstrap';
import Header from "../Header/index";
import {ENGLISH, LANGUAGE_MODE} from "../../constantRoutes";

class CreditsPage extends Component {
    render() {
        return (
            <div>
                {localStorage.getItem(LANGUAGE_MODE) === ENGLISH ? (
                    <div>
                        <Header/>
                        <div className='row'>
                            <div className='col-2'>
                                <Card>
                                    <CardImg
                                        src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
                                        alt="Card image cap"/>
                                    <CardBody>
                                        <CardTitle>Alberto Arencibia</CardTitle>
                                        <CardSubtitle>Author of the images that form the atlas</CardSubtitle>
                                    </CardBody>
                                </Card>
                            </div>
                            <div className='col-2'>
                                <Card>
                                    <CardImg
                                        src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
                                        alt="Card image cap"/>
                                    <CardBody>
                                        <CardTitle>Miguel Ángel</CardTitle>
                                        <CardSubtitle>Responsible for medical chair</CardSubtitle>
                                    </CardBody>
                                </Card>
                            </div>
                            <div className='col-2'>
                                <Card>
                                    <CardImg
                                        src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
                                        alt="Card image cap"/>
                                    <CardBody>
                                        <CardTitle>Carlos Luque</CardTitle>
                                        <CardSubtitle>Tutor</CardSubtitle>
                                    </CardBody>
                                </Card>
                            </div>
                            <div className='col-2'>
                                <Card>
                                    <CardImg
                                        src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
                                        alt="Card image cap"/>
                                    <CardBody>
                                        <CardTitle>Agustín Trujillo</CardTitle>
                                        <CardSubtitle>Tutor</CardSubtitle>
                                    </CardBody>
                                </Card>
                            </div>
                            <div className='col-2'>
                                <Card>
                                    <CardImg
                                        src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
                                        alt="Card image cap"/>
                                    <CardBody>
                                        <CardTitle>Yone Moreno</CardTitle>
                                        <CardSubtitle>Programmer</CardSubtitle>
                                    </CardBody>
                                </Card>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div>
                        <Header/>
                        <div className='row'>
                            <div className='col-2'>
                                <Card>
                                    <CardImg
                                        src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
                                        alt="Card image cap"/>
                                    <CardBody>
                                        <CardTitle>Alberto Arencibia</CardTitle>
                                        <CardSubtitle>Autor de las imágenes que forman el atlas</CardSubtitle>
                                    </CardBody>
                                </Card>
                            </div>
                            <div className='col-2'>
                                <Card>
                                    <CardImg
                                        src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
                                        alt="Card image cap"/>
                                    <CardBody>
                                        <CardTitle>Miguel Ángel</CardTitle>
                                        <CardSubtitle>Responsable cátedra médica</CardSubtitle>
                                    </CardBody>
                                </Card>
                            </div>
                            <div className='col-2'>
                                <Card>
                                    <CardImg
                                        src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
                                        alt="Card image cap"/>
                                    <CardBody>
                                        <CardTitle>Carlos Luque</CardTitle>
                                        <CardSubtitle>Tutor</CardSubtitle>
                                    </CardBody>
                                </Card>
                            </div>
                            <div className='col-2'>
                                <Card>
                                    <CardImg
                                        src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
                                        alt="Card image cap"/>
                                    <CardBody>
                                        <CardTitle>Agustín Trujillo</CardTitle>
                                        <CardSubtitle>Tutor</CardSubtitle>
                                    </CardBody>
                                </Card>
                            </div>
                            <div className='col-2'>
                                <Card>
                                    <CardImg
                                        src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
                                        alt="Card image cap"/>
                                    <CardBody>
                                        <CardTitle>Yone Moreno</CardTitle>
                                        <CardSubtitle>Programador</CardSubtitle>
                                    </CardBody>
                                </Card>
                            </div>
                        </div>
                    </div>
                )}
            </div>

        );
    }
}

export default CreditsPage;

import React, {Component} from 'react';
import logoCatedrasMedicas from './logCatedrasMedicas.PNG';
import Header from "../Header/index";
import {ENGLISH, LANGUAGE_MODE} from "../../constantRoutes";

export default class CatedraMedica extends Component {
    render() {
        return (
            <div>
                {localStorage.getItem(LANGUAGE_MODE) === ENGLISH ?
                    (
                        <div>
                            <Header/>
                            <div className='container'>
                                <img src={logoCatedrasMedicas}/>

                                <h3>Project realized thanks to the scholarship of the
                                    <span className="badge badge-success">Medical Chair</span>
                                </h3>

                                <div className="mt-10">
                                    <a href="https://catedratecnologiamedica.ulpgc.es/"
                                       className="badge badge-info">More information</a>
                                </div>
                            </div>
                        </div>
                    )
                    :
                    (
                        <div>
                            <Header/>
                            <div className='container'>
                                <img src={logoCatedrasMedicas}/>

                                <h3>Proyecto realizado gracias a la beca de la <span className="badge badge-success">Cátedra
                        Médica</span>
                                </h3>

                                <div className="mt-10">
                                    <a href="https://catedratecnologiamedica.ulpgc.es/"
                                       className="badge badge-info">Más información</a>
                                </div>

                            </div>
                        </div>
                    )
                }
            </div>
        )
    }
}

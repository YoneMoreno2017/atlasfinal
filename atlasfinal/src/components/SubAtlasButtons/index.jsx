import React, {Component} from 'react';
import {Button} from "reactstrap";
import {
    BACKBONE_PAGE,
    ENGLISH,
    HEAD_PAGE,
    LANGUAGE_MODE,
    PELVIS_PAGE,
    RIB_PAGE,
    TORAX_PAGE
} from "../../constantRoutes";

class SubAtlasButtons extends Component {

    constructor() {
        super();
        this.state = ({inEnglish: false});

        this.changeSubatlasTextToEnglish = this.changeSubatlasTextToEnglish.bind(this);
        this.changeSubatlasTextToSpanish = this.changeSubatlasTextToSpanish.bind(this);
        window.changeSubatlasTextToEnglish = this.changeSubatlasTextToEnglish;
        window.changeSubatlasTextToSpanish = this.changeSubatlasTextToSpanish;
    }

    changeSubatlasTextToEnglish() {
        this.setState(() => ({inEnglish: true}));
    }

    changeSubatlasTextToSpanish() {
        this.setState(() => ({inEnglish: false}));
    }

    render() {
        let english = this.state.inEnglish;
        return (
            <div>
                {english || localStorage.getItem(LANGUAGE_MODE) === ENGLISH ? (
                        <div>
                            <h6>Choose the subatlas to load:</h6>
                            <div className="row">
                                <div className="col-6">
                                    <a href={BACKBONE_PAGE}>
                                        <Button
                                            className='ml-3 btn-sm' color="danger"> Backbone
                                        </Button>
                                    </a>
                                </div>
                                <div className="col-6">
                                    <a href={HEAD_PAGE}> <Button
                                        className='mr-3 btn-sm' color="info">Head
                                    </Button>
                                    </a>
                                </div>
                            </div>
                            <br/>
                            <div className="row">
                                <div className="col-3">
                                    <a href={TORAX_PAGE}>
                                        <Button
                                            className='ml-3 btn-sm' color="success"> Torax
                                        </Button>
                                    </a>
                                </div>
                                <a href={RIB_PAGE}>
                                    <div className="col-3">
                                        <Button
                                            className='mr-3 btn-sm' color="warning">Ribs
                                        </Button>
                                    </div>
                                </a>
                                <a href={PELVIS_PAGE}>
                                    <div className="col-6">
                                        <Button
                                            className=' mr-3 btn-sm' color="primary">Pelvis
                                        </Button>
                                    </div>
                                </a>
                            </div>
                        </div>

                    ) :
                    (
                        <div>
                            <h6>Elija el subatlas a cargar:</h6>
                            <div className="row">
                                <div className="col-6">
                                    <a href={BACKBONE_PAGE}>
                                        <Button
                                            className='ml-3 btn-sm' color="danger"> Columna
                                        </Button>
                                    </a>
                                </div>
                                <div className="col-6">
                                    <a href={HEAD_PAGE}> <Button
                                        className='mr-3 btn-sm' color="info">Cabeza
                                    </Button>
                                    </a>
                                </div>
                            </div>
                            <br/>
                            <div className="row">
                                <div className="col-3">
                                    <a href={TORAX_PAGE}>
                                        <Button
                                            className='ml-3 btn-sm' color="success"> Torácico
                                        </Button>
                                    </a>
                                </div>
                                <a href={RIB_PAGE}>
                                    <div className="col-3">
                                        <Button
                                            className='mr-3 btn-sm' color="warning">Costillas
                                        </Button>
                                    </div>
                                </a>
                                <a href={PELVIS_PAGE}>
                                    <div className="col-6">
                                        <Button
                                            className=' mr-3 btn-sm' color="primary">Pelviano
                                        </Button>
                                    </div>
                                </a>
                            </div>
                        </div>
                    )
                }

            </div>
        );
    }
}

export default SubAtlasButtons;
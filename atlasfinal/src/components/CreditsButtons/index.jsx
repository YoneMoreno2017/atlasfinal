import React, {Component} from 'react';
import {Button} from "reactstrap";
import {CATEDRA_MEDICA_PAGE, CREDITS_PAGE, ENGLISH, LANGUAGE_MODE} from "../../constantRoutes";

class CreditsButtons extends Component {

    constructor(props) {
        super();
        this.state = ({inEnglish: false});

        this.changeCreditsToEnglish = this.changeCreditsToEnglish.bind(this);
        this.changeCreditsToSpanish = this.changeCreditsToSpanish.bind(this);
        window.changeCreditsToEnglish = this.changeCreditsToEnglish;
        window.changeCreditsToSpanish = this.changeCreditsToSpanish;
    }

    changeCreditsToEnglish() {
        this.setState(() => ({inEnglish: true}))
    }

    changeCreditsToSpanish() {
        this.setState(() => ({inEnglish: false}))
    }


    render() {
        let english = this.state.inEnglish;
        return (
            <div>
                {english || localStorage.getItem(LANGUAGE_MODE) === ENGLISH ? (
                    <div>
                        <h6>Other pages:</h6>
                        <div className="row">
                            <div className="col-3">
                                <a href={CREDITS_PAGE}>
                                    <Button
                                        className='ml-3 mr-3 btn-sm' color="primary"> Credits
                                    </Button>
                                </a>
                            </div>
                            <div className="col-3">
                                <a href={CATEDRA_MEDICA_PAGE}>
                                    <Button
                                        className='ml-3 mr-3 btn-sm' color="primary">Medical Chair
                                    </Button>
                                </a>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div>
                        <h6>Otras páginas:</h6>
                        <div className="row">
                            <div className="col-3">
                                <a href={CREDITS_PAGE}>
                                    <Button
                                        className='ml-3 mr-3 btn-sm' color="primary"> Créditos
                                    </Button>
                                </a>
                            </div>
                            <div className="col-3">
                                <a href={CATEDRA_MEDICA_PAGE}>
                                    <Button
                                        className='ml-3 mr-3 btn-sm' color="primary">Cátedra Médica
                                    </Button>
                                </a>
                            </div>
                        </div>
                    </div>
                )}
            </div>

        );
    }
}

export default CreditsButtons;
/*It has the div which links React and Three*/
import React, {Component} from 'react';
import threeEntryPoint from '../threejs/threeEntryPoint';
import spinner from './spinner.gif';
import './styles.css';

export default class NrrdCanvas extends Component {
    constructor() {
        super();

        this.state = {rebuild: false};

        window.rebuildNrrdCanvas = this.rebuildNrrdCanvas.bind(this);
    }

    rebuildNrrdCanvas() {


        this.setState({rebuild: true});

        if (window.multiVolumeSlice) {
            window.multiVolumeSlice = null;
        }

        window.IndexAtlas.lastSegmentClicked = null;
        window.IndexAtlas.currentSegmentClicked = null;
        window.IndexAtlas.indexesOfClickedSegments = [];

        document.getElementsByTagName('canvas')[0].remove();

        new threeEntryPoint(this.threeRootElement,
            this.props.IndexAtlas);


    }


    componentDidMount() {
        new threeEntryPoint(this.threeRootElement,
            this.props.IndexAtlas);
    }


    render() {
        return (
            <div
                id='originalNrrd'
                className='nrrdCanvas'
                style={
                    {
                        width: this.props.width,
                        height: this.props.height,
                        backgroundColor: 'black'
                    }}
                ref={element => this.threeRootElement = element}
            >
                <img
                    className='spinner' //Required: we use it to removeSpinners() in SceneSubject
                    src={spinner}
                />
            </div>
        );
    }
}
import React, {Component} from 'react';
import {Button} from "reactstrap";
import SplitterLayout from 'react-splitter-layout';
import SubAtlasButtons from "../SubAtlasButtons/index";
import CreditsButtons from "../CreditsButtons/index";


export default class AtlasButtons extends Component {
    render() {
        return (
            <div>
                <SplitterLayout
                    vertical={false}
                    percentage={true}
                    secondaryInitialSize={35}
                    secondaryMinSize={35}
                    primaryMinSize={65}
                >
                    <SubAtlasButtons/>
                    <CreditsButtons/>
                </SplitterLayout>
            </div>
        );
    }
}
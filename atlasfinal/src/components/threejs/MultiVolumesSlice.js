/*It has the responsability of merging two VolumeSlices into a mesh, where we have a background,
which is in gray colors scale, and labels which are colors
 */

import * as THREE from "three";
import VolumeSlice from "./VolumeSlice";
import Volume from "./Volume";

/**
 * This class has been made to merge several slices
 * @class
 * @see THREE.MultiVolumesSlice
 */

const MultiVolumesSlice = function () {

    const self = this;

    function definePropertyAsFirstSlice(name) {
        Object.defineProperty(self, name, {
            get: function () {
                if (self.slices.length > 0) {
                    return this.slices[0][name];
                }
                return undefined;

            },
            set: function () {

            }
        });
    }


    /**
     * @member {Array} slices Hold all the slices to merge
     */
    this.slices = [];

    /**
     * @member {Array} opacities Hold all the corresponding opacities
     */
    this.opacities = [];

    /**
     * @member {Array} visibilities Hold all the corresponding visibilities
     */
    this.visibilities = [];

    /**
     * @member {Array} volumes Read only : Hold all the volumes associated to the slices
     */
    Object.defineProperty(this, 'volumes', {
        get: function () {
            return this.slices.map(slice => slice.volume);
        },
        set: function () {
        }
    });

    /**
     * @member {Number} index index of all the slice (should be consistent)
     */
    Object.defineProperty(this, 'index', {
        get: function () {

            if (this.slices.length > 0) {
                return this.slices[0].index;
            }
            return undefined;

        },
        set: function (value) {

            value = Number(value);
            if (!isNaN(value)) {
                this.slices.forEach(slice => slice.index = value);
                this.geometryNeedsUpdate = true;
            }
            return value;

        }
    });
    /**
     * @member {String} axis The normal axis
     */
    definePropertyAsFirstSlice('axis');


    /**
     * @member {Object} listeners store all the listeners to the events of this slice
     */
    this.listeners = {
        repaint: [],
        addSlice: [],
        removeSlice: [],
        updateGeometry: []
    };

    /**
     * @member {HTMLCanvasElement} canvas The final canvas used for the texture
     */
    /**
     * @member {CanvasRenderingContext2D} ctx Context of the canvas
     */
    this.canvas = document.createElement('canvas');

    this.alphaCanvas = document.createElement('canvas');


    const canvasMap = new THREE.Texture(this.canvas);
    canvasMap.minFilter = THREE.LinearFilter;
    canvasMap.wrapS = canvasMap.wrapT = THREE.ClampToEdgeWrapping;

    const alphaCanvasMap = new THREE.Texture(this.alphaCanvas);
    alphaCanvasMap.minFilter = THREE.LinearFilter;
    alphaCanvasMap.wrapS = alphaCanvasMap.wrapT = THREE.ClampToEdgeWrapping;

    // var material = new THREE.MeshBasicMaterial({
    //     map: canvasMap,
    //     side: THREE.DoubleSide, transparent: true, alphaTest: 0.00
    // });

    const material = new THREE.MeshBasicMaterial({
        map: canvasMap, alphaMap: alphaCanvasMap,
        side: THREE.DoubleSide, transparent: true, alphaTest: 0.00
    });
    /**
     * @member {THREE.Mesh} mesh The mesh ready to get used in the scene
     */
    this.mesh = new THREE.Mesh(this.geometry, material);
    this.mesh.renderOrder = 0;
    /**
     * @member {Boolean} geometryNeedsUpdate If set to true, updateGeometry will be triggered at the next repaint
     */
    this.geometryNeedsUpdate = true;

    /**
     * @member {Number} iLength Width of slice in the original coordinate system, corresponds to the width of the buffer canvas
     */
    definePropertyAsFirstSlice('iLength');

    /**
     * @member {Number} jLength Height of slice in the original coordinate system, corresponds to the height of the buffer canvas
     */

    definePropertyAsFirstSlice('jLength');

    definePropertyAsFirstSlice('matrix');
    definePropertyAsFirstSlice('maxIndex');


};


MultiVolumesSlice.prototype = {

    constructor: MultiVolumesSlice,

    /**
     * @member {Function} repaint Refresh the texture and the geometry if geometryNeedsUpdate is set to true
     * @param {Boolean} repaintAll Introduced to avoid unnecesary redraw of every sub slice. If not specified, sub slices will not be repainted.
     * @param repaintLabel
     * @param repaintBackground
     * @memberof THREE.MultiVolumesSlice
     */
    repaint: function (repaintAll, repaintLabel, repaintBackground) {


        repaintAll = repaintAll || false;
        if (repaintAll) {
            this.repaintAll();
        } else {
            if (repaintLabel) {
                this.repaintLabel();
            } else {
                if (repaintBackground) {
                    this.repaintBackground();
                }
            }

        }

        if (this.geometryNeedsUpdate) {


            this.slices[0].geometryNeedsUpdate = true;
            this.updateGeometry();

        }

        let i,
            slice;
        const ctx = this.ctx;

        //clean canvas before doing anything
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        // console.warn('MultiVolumeSlice::repaint:: dibuja slice canvas');
        for (i = 0; i < this.slices.length; i++) {
            slice = this.slices[i];
            if (this.visibilities[i] && this.opacities[i] > 0) {
                ctx.globalAlpha = this.opacities[i];

            }

        }


        //Permite que las zonas de colores con alpha a 0 sean transparentes y dejen ver el fondo

        // console.info('Entra en MultiVolumeSlice para convertir la zona con alpha a 0 a transparente');

        const imageData = ctx.getImageData(0, 0, this.canvas.width, this.canvas.height).data;
        const alphaCtx = this.alphaCanvas.getContext('2d');
        const alphaImageData = alphaCtx.getImageData(0, 0, this.canvas.width, this.canvas.height);
        const alphaData = alphaImageData.data;
        for (i = 0; i < alphaData.length;) {
            alphaData[i] = imageData[i + 3];
            alphaData[i + 1] = imageData[i + 3];
            alphaData[i + 2] = imageData[i + 3];
            alphaData[i + 3] = imageData[i + 3];
            i = i + 4;
        }
        alphaCtx.putImageData(alphaImageData, 0, 0);
        // console.log('alphaData', alphaData);

        // console.warn('MultiVolumeSlice::mesh.material.needsUpdate');


        this.mesh.material.alphaMap.needsUpdate = true;
        this.mesh.material.map.needsUpdate = true;

        this.listeners.repaint.map(listener => listener.callback.call(listener.context));
        return;

        function flipImage(canvas, ctx, slice) {


            ctx.save();

            ctx.translate(canvas.width / 2, canvas.height / 2);
            ctx.scale(-1, 1);

            ctx.drawImage(slice.canvas, -canvas.width / 2,
                -canvas.height / 2, canvas.width, canvas.height);
            ctx.restore();
        }

    },

    /**
     * @member {Function} updateGeometry Refresh the geometry according to axis and index
     * @see THREE.Volume.extractPerpendicularPlane
     * @memberof THREE.MultiVolumesSlice
     */
    updateGeometry: function () {

        if (this.slices.length > 0) {

            const mainSlice = this.slices[0];

            this.canvas.width = mainSlice.canvas.width;
            this.canvas.height = mainSlice.canvas.height;
            this.ctx = this.canvas.getContext('2d');

            this.alphaCanvas.width = mainSlice.canvas.width;
            this.alphaCanvas.height = mainSlice.canvas.height;

            this.geometry = mainSlice.geometry;

            if (this.mesh) {

                this.mesh.geometry = this.geometry;
                //reset mesh matrix
                this.mesh.matrix = ( new THREE.Matrix4() ).identity();
                this.mesh.applyMatrix(this.matrix);

            }

            this.geometryNeedsUpdate = false;
            this.listeners.updateGeometry.map(listener => listener.callback.call(listener.context));
        }

    },

    /**
     * @member {Function} onRepaint add a listener to the list of listeners
     * @param {Object} context
     * @param callback
     * @memberof THREE.MultiVolumesSlice
     */
    onRepaint: function (context, callback) {

        this.listeners.repaint.push({callback: callback, context: context});

    },

    /**
     * @member {Function} onAddSlice add a listener to the list of listeners
     * @param {Object} context
     * @param callback
     * @memberof THREE.MultiVolumesSlice
     */
    onAddSlice: function (context, callback) {

        this.listeners.addSlice.push({callback: callback, context: context});

    },

    /**
     * @member {Function} onRemoveSlice add a listener to the list of listeners
     * @param {Object} context
     * @param callback
     * @memberof THREE.MultiVolumesSlice
     */
    onRemoveSlice: function (context, callback) {

        this.listeners.removeSlice.push({callback: callback, context: context});

    },


    /**
     * @member {Function} onUpdateGeometry add a listener to the list of listeners
     * @param {Object} context
     * @param callback
     * @memberof THREE.MultiVolumesSlice
     */
    onUpdateGeometry: function (context, callback) {

        this.listeners.updateGeometry.push({callback: callback, context: context});

    },

    /**
     * @member {Function} addSlice add a slice to the list of slices to merge
     * @param {THREE.VolumeSlice} slice           The slice to add
     * @param {Number}            opacity         The opacity associated to this layer. Default is 1.
     * @param {Boolean} insertInBackground If true the slice will be the new background
     * @memberof THREE.MultiVolumesSlice
     */

    //Vemos que si insertInBackground es true ello significa que se pone en lo alto de la estructura
    //y lo cargamos como el fondo en niveles de grises.
    //En caso de que esta no sea un fondo se pone por el final de la estructura, por la derecha.

    addSlice: function (slice, opacity, insertInBackground) {


        if (!this.slices.includes(slice)) {
            opacity = opacity === undefined ? 1 : opacity;
            /*console.log('opacidad puesta en MVS',
                opacity, this);*/
            insertInBackground = insertInBackground || false;

            if (insertInBackground) {
                this.slices.unshift(slice);
                this.opacities.unshift(opacity);
                this.visibilities.unshift(true);
            }
            else {
                this.slices.push(slice);
                this.opacities.push(opacity);
                this.visibilities.push(true);
            }

            // console.warn('MultiVolumeSLice::addSlice::slices.length', this.slices.length);

            this.listeners.addSlice.map(listener => listener.callback.call(listener.context, slice));
        }
    },

    /**
     * @member {Function} removeSlice remove a slice from the list of slices to merge
     * @param {THREE.VolumeSlice} slice           The slice to remove
     * @memberof THREE.MultiVolumesSlice
     */
    removeSlice: function (slice) {

        const index = this.slices.indexOf(slice);
        if (index > -1) {
            this.slices.splice(index, 1);
            this.opacities.splice(index, 1);

            this.listeners.removeSlice.map(listener => listener.callback.call(listener.context, slice));
        }

    },

    /**
     * @member {Function} setOpacity change the opacity of the given slice
     * @param {THREE.VolumeSlice} slice   The slice or volume whose opacity will be changed
     * @param {Number} opacity  new value
     * @memberof THREE.MultiVolumesSlice
     */
    setOpacity: function (slice, opacity) {
        let index;
        if (slice instanceof VolumeSlice) {
            index = this.slices.indexOf(slice);
            if (index > -1) {
                this.opacities[index] = opacity;
            }
        }
        else if (slice instanceof Volume) {
            index = this.volumes.indexOf(slice);
            if (index > -1) {
                this.opacities[index] = opacity;
            }
        }

    },

    /**
     * @member {Function} getOpacity get the opacity of the given slice
     * @param {THREE.VolumeSlice} slice   The slice or volume
     * @memberof THREE.MultiVolumesSlice
     * @returns {undefined} the opacity
     */
    getOpacity: function (slice) {
        let index;
        if (slice instanceof VolumeSlice) {
            index = this.slices.indexOf(slice);
            if (index > -1) {
                return this.opacities[index];
            }
        }
        else if (slice instanceof Volume) {
            index = this.volumes.indexOf(slice);
            if (index > -1) {
                return this.opacities[index];
            }
        }
        return undefined;

    },

    /**
     * @member {Function} setVisibility change the visibility of the given slice or volume
     * @param {THREE.VolumeSlice} slice   The slice or volume whose visibility will be changed
     * @param {Number} visibility  new value
     * @memberof THREE.MultiVolumesSlice
     */
    setVisibility: function (slice, visibility) {

        let index;
        if (slice instanceof VolumeSlice) {
            index = this.slices.indexOf(slice);
            if (index > -1) {
                this.visibilities[index] = visibility;
            }
        }
        else if (slice instanceof Volume) {
            index = this.volumes.indexOf(slice);
            if (index > -1) {
                this.visibilities[index] = visibility;
            }
        }

    },

    /**
     * @member {Function} getVisibility get the visibility of the given slice or volume
     * @param {THREE.VolumeSlice} slice   The slice or volume
     * @memberof THREE.MultiVolumesSlice
     * @returns {undefined} the visibility
     */
    getVisibility: function (slice) {
        let index;
        if (slice instanceof VolumeSlice) {
            index = this.slices.indexOf(slice);
            if (index > -1) {
                return this.visibilities[index];
            }
        }
        else if (slice instanceof Volume) {
            index = this.volumes.indexOf(slice);
            if (index > -1) {
                return this.visibilities[index];
            }
        }
        return undefined;

    },

    /**
     * @member {Function} getBackground get the background of this multislice
     * @memberof THREE.MultiVolumesSlice
     * @returns {THREE.Volume} the background or null if none is found
     */

    //Esta funcion nos podria servir para recuperaar el fondo

    getBackground: function () {
        const volumes = this.volumes;
        for (let i = 0; i < volumes.length; i++) {
            const background = volumes[i];
            // console.error('MultiVolumeSlice::background', background);
            if (background.dataType === 'background' && this.getOpacity(background) > 0) {
                // console.error('MultiVolumeSlice::background', background);
                return background;
            }
        }
        return null;
    },


    /**
     * @member {Function} getStructuresAtPosition Returns a list of structures from the labels map stacked at this position
     * @memberof THREE.MultiVolumesSlice
     * @returns {{i: number, j: number}} the structures (can contain undefined)
     * @param IJx
     * @param IJy
     * @param slice
     */
    getStructuresAtPosition: function (IJx, IJy, slice) {

        const i = Math.round(IJx * slice.canvasBuffer.width / slice.canvas.width);
        const j = Math.round(IJy * slice.canvasBuffer.height / slice.canvas.height);


        if (i >= slice.iLength || i < 0 || j >= slice.jLength || j < 0) {
            return undefined;
        }
        return {i, j};
    },

    getLabelSlice: function () {
        return this.slices[this.slices.length - 1];
    },


    repaintAll: function () {
        this.slices.forEach(function (slice) {
            slice.repaint();

        });
    },
    repaintLabel: function () {
        this.slices.forEach(function (slice) {
            if (isLabel(slice)) { //Esto es nuestro
                slice.repaint();
            }
        });
    },
    repaintBackground: function () {
        this.slices.forEach(function (slice) {
            if (slice.volume.dataType === 'background') { //Esto es nuestro
                slice.repaint();
            }
        });
    },

};


let isLabel = function (slice) {
    return slice.volume.dataType === 'label';
};

export default MultiVolumesSlice;
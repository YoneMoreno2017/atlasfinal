/*threeEntryPoint generates the canvas and the controller: SceneManager*/

import SceneManager from './SceneManager';


export default class threeEntryPoint {

    constructor(container, IndexAtlas) {
        this.canvas = this.createCanvas(document, container);
        this.sceneManager = new SceneManager(this.canvas, IndexAtlas);

        this.canvasHalfWidth;
        this.canvasHalfHeight;


        this.render = this.render.bind(this);
        this.resizeCanvas = this.resizeCanvas.bind(this);
        this.render();

        this.bindEventListeners();
    }


    createCanvas(document, container) {
        const canvas = document.createElement('canvas');
        container.appendChild(canvas);
        return canvas;
    }


    bindEventListeners() {
        window.onresize = this.resizeCanvas;
        this.resizeCanvas();
    }


    resizeCanvas() {
        this.canvas.style.width = '100%';
        this.canvas.style.height = '100%';

        this.canvas.width = this.canvas.offsetWidth;
        this.canvas.height = this.canvas.offsetHeight;

        this.canvasHalfWidth = Math.round(this.canvas.offsetWidth / 2);
        this.canvasHalfHeight = Math.round(this.canvas.offsetHeight / 2);

        this.sceneManager.onWindowResize()
    }


    render(time) {
        requestAnimationFrame(this.render);
        this.sceneManager.update();
    }
}
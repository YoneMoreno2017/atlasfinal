/*SceneManager is responsible for building all THREE's elements into the scene*/
import SceneSubject from './SceneSubject';
import * as THREE from "three";
import TrackballControls from './TrackballControls';


export default class SceneManager {
    constructor(canvas, IndexAtlas) {
        this.canvas = canvas;
        this.clock = new THREE.Clock();

        this.screenDimensions = {
            width: canvas.width,
            height: canvas.height
        };

        this.scene = this.buildScene();
        this.renderer = this.buildRender(this.screenDimensions);
        this.camera = this.buildCamera(this.screenDimensions);
        this.controls = this.buildControls(this.camera, this.canvas);
        this.sceneSubjects = this.createSceneSubjects(this.scene, IndexAtlas);

    }

    buildScene() {
        const scene = new THREE.Scene();
        scene.background = new THREE.Color("#000000");

        return scene;
    }

    buildRender({width, height}) {
        const renderer = new THREE.WebGLRenderer({canvas: this.canvas, antialias: true, alpha: true});
        const DPR = window.devicePixelRatio ? window.devicePixelRatio : 1;
        renderer.setPixelRatio(DPR);
        renderer.setSize(width, height);

        renderer.gammaInput = true;
        renderer.gammaOutput = true;

        return renderer;
    }

    buildCamera({width, height}) {
        const aspectRatio = width / height;
        const fieldOfView = 60;
        const nearPlane = 4;
        const farPlane = 1000;
        const camera = new THREE.PerspectiveCamera(fieldOfView, aspectRatio, nearPlane, farPlane);

        camera.position.z = 300;

        window.camera = camera;

        return camera;
    }

    buildControls(camera, canvas) {
        const controls = new TrackballControls(camera, canvas);
        return enableZoomAndPan();
        return;

        function enableZoomAndPan() {
            controls.zoomSpeed = 1.2;
            controls.panSpeed = -0.8;

            controls.noZoom = false;
            controls.noPan = false;
            controls.noRotate = true;

            controls.staticMoving = true;
            controls.dynamicDampingFactor = 0.3;

            return controls;
        }

    }

    createSceneSubjects(scene, IndexAtlas) {


        let isOriginalNrrd = true;
        let sceneSubjects = [
            new SceneSubject(scene, isOriginalNrrd, IndexAtlas, 'original')
        ];
        window.sceneSubjects = sceneSubjects;
        return window.sceneSubjects;
    }


    update() {
        const elapsedTime = this.clock.getElapsedTime();

        this.controls.update();

        for (let i = 0; i < this.sceneSubjects.length; i++)
            this.sceneSubjects[i].update(elapsedTime);
        this.renderer.render(this.scene, this.camera);
    }


    onWindowResize() {
        const {width, height} = this.canvas;

        this.screenDimensions.width = width;
        this.screenDimensions.height = height;

        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize(width, height);
    }

}
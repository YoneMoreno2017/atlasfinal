import Color from "./Color";
import MultiVolumesSlice from "./MultiVolumesSlice";

export default class VolumesManager {
    constructor(loader) {
        this.loader = loader;
        this.color = new Color();
    }


    addVolumeSliceToMultiVolumeSlice(sliceZ) {


        if (isMultiVolumeSliceUndefined()) {
            addBackground(sliceZ, this.loader);
        } else {
            addLabel(sliceZ, this.color);
        }


        function isMultiVolumeSliceUndefined() {
            return !window.multiVolumeSlice;
        }


        function addBackground(sliceZ, loader) {
            sliceZ.dataType = 'background';
            window.multiVolumeSlice = new MultiVolumesSlice();
            insertBackgroundInMultiVolumeSlice(sliceZ, loader);
        }


        function insertBackgroundInMultiVolumeSlice(sliceZ) {
            sliceZ.volume.dataType = 'background';
            const insertInBackground = true;
            const opacity = 1;
            window.multiVolumeSlice.addSlice(sliceZ, opacity, insertInBackground);


        }

        function addLabel(sliceZ, color) {
            sliceZ.volume.dataType = 'label';
            color.setColorMap(sliceZ);
            insertColorInMultiVolumeSlice(sliceZ);
            window.multiVolumeSlice.repaint(true, false, false);


            function insertColorInMultiVolumeSlice(sliceZ) {
                const insertInBackground = false;
                const opacity = 0;
                window.multiVolumeSlice.addSlice(sliceZ, opacity, insertInBackground);
            }
        }


    }
}
import {CLICKED_SEGMENT_ALPHA} from "../../constantRoutes";

export default class Color {
    setColorMap(sliceZ) {

        const colors = window.IndexAtlas.getSubAtlasActual().getParteActual().getSegmentos().map((segmento => segmento.color));

        generateArrayColors(sliceZ, colors, 0);

        sliceZ.colorMap = sliceZ.arrayColors.map((color) => this.colorToHex(color));


        function generateArrayColors(sliceZ, colors, alpha) {
            sliceZ.arrayColors = colors.map
            ((color) =>
                `rgba(${color.replace('"', '').replace(/ /g, ',').replace(/\s/g, '')},${alpha})`);
        }
    }

    colorClickedSegment(labelSlice, indexOfClickedSegment) {
        labelSlice.arrayColors.splice(indexOfClickedSegment, 1,
            putAlpha(labelSlice.arrayColors[indexOfClickedSegment]));

        labelSlice.colorMap = labelSlice.arrayColors.map((color) => this.colorToHex(color));

        window.onSegmentListItemClickFunction(indexOfClickedSegment);


        function putAlpha(clickedSegmentColor) {
            const indexOfLastComma = clickedSegmentColor.lastIndexOf(',');
            return setCharAt(clickedSegmentColor, indexOfLastComma + 1, CLICKED_SEGMENT_ALPHA);

            function setCharAt(str, index, chr) {
                if (index > str.length - 1) return str;
                return str.substr(0, index) + chr + str.substr(index + 1);
            }
        }
    }

    deleteClickedSegmentColor(labelSlice) {


        labelSlice.arrayColors.splice(window.IndexAtlas.getLastSegmentClicked(), 1,
            deleteAlpha(labelSlice.arrayColors[window.IndexAtlas.getLastSegmentClicked()]));


        labelSlice.colorMap = labelSlice.arrayColors.map((color) => this.colorToHex(color));


        function deleteAlpha(clickedSegmentColor) {
            const indexOfLastComma = clickedSegmentColor.lastIndexOf(',');
            return replaceAt(
                clickedSegmentColor, CLICKED_SEGMENT_ALPHA, 0, indexOfLastComma, clickedSegmentColor.length);

            function replaceAt(input, search, replace, start, end) {
                return input.slice(0, start)
                    + input.slice(start, end).replace(search, replace)
                    + input.slice(end);
            }
        }
    }


    colorToHex(color, opacity) {
        let match,
            r,
            g,
            b,
            a;
        if (typeof color === 'string') {
            const rgb = /^rgb *\( *(\d+) *, *(\d+) *, *(\d+) *\)$/;
            const rgba = /^rgba *\( *(\d+) *, *(\d+) *, *(\d+) *, *(\d*\.?\d*) *\)$/;
            match = color.match(rgb) || color.match(rgba);
            if (match) {
                r = Number(match[1]);
                g = Number(match[2]);
                b = Number(match[3]);
                a = Number(opacity || match[4] || 255);
                if (a <= 1) {
                    a = Math.round(a * 255);
                }
                return (r << 24) + (g << 16) + (b << 8) + a;
            }
        }
        throw 'Application did not manage to parse a color from : ' + color;
    }


}
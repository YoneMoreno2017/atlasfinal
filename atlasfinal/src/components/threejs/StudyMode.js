import Color from "./Color";
import {REVIEW, STUDY, STUDY_MODE} from "../../constantRoutes";

export default class StudyMode {
    constructor() {
        this.mode = localStorage.getItem(STUDY_MODE) || REVIEW;
        this.color = new Color();
    }

    getMode() {
        return this.mode;
    }

    applyStudyMode(labelSlice, indexOfClickedSegment) {

        if (this.getMode() === REVIEW) {

            if (this.currentSegmentClickedIsNotInTheClickedSegmentsList(indexOfClickedSegment)) {

                this.insertIntoListOfPreviousClickedSegments(indexOfClickedSegment);

                this.color.colorClickedSegment(labelSlice, indexOfClickedSegment);

                window.multiVolumeSlice.repaint(false, true, false);

            }

        } else {
            if (this.getMode() === STUDY) {


                if (this.currentSegmentClickedIsNotInTheClickedSegmentsList(indexOfClickedSegment)) {

                    this.letPreviousSegmentClickedToBeColoredAgainAftherCurrentClick();

                    this.insertIntoListOfPreviousClickedSegments(indexOfClickedSegment);

                    if (this.isCurrentSegmentClickedDifferentTo(indexOfClickedSegment)) {

                        this.color.colorClickedSegment(labelSlice, indexOfClickedSegment);

                        window.IndexAtlas.setCurrentSegmentClicked(indexOfClickedSegment);

                        if (window.IndexAtlas.getLastSegmentClicked()) {
                            this.color.deleteClickedSegmentColor(labelSlice);
                        }
                        window.IndexAtlas.setLastSegmentClicked(window.IndexAtlas.getCurrentSegmentClicked());
                        window.multiVolumeSlice.repaint(false, true, false);
                    }
                }
            }
        }

    }


    isCurrentSegmentClickedDifferentTo(indexOfClickedSegment) {
        return window.IndexAtlas.getCurrentSegmentClicked() !== indexOfClickedSegment;
    }

    insertIntoListOfPreviousClickedSegments(indexOfClickedSegment) {
        window.IndexAtlas.getIndexesOfClickedSegments().push(indexOfClickedSegment);
    }

    letPreviousSegmentClickedToBeColoredAgainAftherCurrentClick() {
        if (window.IndexAtlas.getIndexesOfClickedSegments().length === 1) {
            window.IndexAtlas.getIndexesOfClickedSegments().pop();
        }
    }

    currentSegmentClickedIsNotInTheClickedSegmentsList(indexOfClickedSegment) {
        return !window.IndexAtlas.getIndexesOfClickedSegments().includes(indexOfClickedSegment);
    }
}
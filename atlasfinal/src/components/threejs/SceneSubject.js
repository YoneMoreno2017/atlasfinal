/*SceneSubject is responsible for loading the volume to display in the canvas
* In addition it puts the background/label into the MultiVolumeSlice.
* It also puts the colorMap into the label
* */

import NRRDLoader from "../../loaders/NRRDLoader";
import VolumesManager from "./VolumesManager";


export default class SceneSubject {
    constructor(scene, originalNrrd, IndexAtlas, name, sceneSubjects) {

        this.scene = scene;
        this.originalNrrd = originalNrrd;

        this.loader = new NRRDLoader();
        this.filename = this.getFilename(IndexAtlas, originalNrrd);

        this.volumesManager = new VolumesManager(this.loader);

        this.loader.load(this.filename, this.onSuccess.bind(this));

        this.name = name;

        this.sceneSubjects = sceneSubjects
    }

    onSuccess(volume) {
        //z plane
        let sliceZ = volume.extractSlice('z', Math.floor(volume.RASDimensions[2] / 4));

        this.volumesManager.addVolumeSliceToMultiVolumeSlice(sliceZ, this.originalNrrd);

        this.handleLoadingSpinners();

        this.scene.add(sliceZ.mesh);

        this.loadSegmentedAndColors();
    }


    loadSegmentedAndColors() {
        if (this.name === 'original') {
            this.adjustCameraPositionToCanvasWidth();
            window.sceneSubjects.push(new SceneSubject(this.scene, false, window.IndexAtlas, 'segmented'));
        }
        if (this.name === 'segmented') {
            window.sceneSubjects.push(new SceneSubject(this.scene, false, window.IndexAtlas, 'colors'));
        }
    }

    adjustCameraPositionToCanvasWidth() {
        window.camera.position.z = Math.min(
            window.multiVolumeSlice.canvas.width,
            window.multiVolumeSlice.canvas.height
        ) + 50;
    }

    getFilename(IndexAtlas, originalNrrd) {

        if (partHasBeenChosenFromGalery()) {
            return getChoosenPartFromThePartClickedOnTheGallery();
        } else {
            return getChoosenPartFromSegmentClickedOnCoverPageURL();
        }


        function partHasBeenChosenFromGalery() {
            return window.partIndex != null;
        }

        function getChoosenPartFromThePartClickedOnTheGallery() {
            return originalNrrd ?
                IndexAtlas.getSubAtlasActual().subAtlas[window.partIndex].getNrrdVolumen().getOriginal()
                :
                IndexAtlas.getSubAtlasActual().subAtlas[window.partIndex].getNrrdVolumen().getSegmentado();
        }

        function getChoosenPartFromSegmentClickedOnCoverPageURL() {
            return originalNrrd ?
                IndexAtlas.getSubAtlasActual().getParteActual().getNrrdVolumen().getOriginal()
                :
                IndexAtlas.getSubAtlasActual().getParteActual().getNrrdVolumen().getSegmentado();
        }
    }


    handleLoadingSpinners() {
        if (isThereASpinner()) {
            removeSpinners();
        }

        function isThereASpinner() {
            return document.getElementsByClassName('spinner')[0];
        }

        function removeSpinners() {
            const spinners = document.getElementsByClassName('spinner');
            spinners[0].remove();
        }
    }

    update(time) {

    }


}


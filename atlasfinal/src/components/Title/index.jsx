/*SHows the NRRDs title*/
import React, {Component} from 'react';
import './styles.css';

class Title extends Component {
    render() {
        return (
            <div id={this.props.id}>{this.props.title}</div>
        );
    }
}


export default Title;
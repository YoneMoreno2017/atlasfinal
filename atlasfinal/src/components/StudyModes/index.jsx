/*The students can select between Estudio, and Repaso, two ways of color Scene*/
import React, {Component} from 'react';
import {Button, ButtonGroup} from 'reactstrap';
import Alert from "../Alert/index";
import {ENGLISH, LANGUAGE_MODE, REVIEW, STUDY, STUDY_MODE} from "../../constantRoutes";


class StudyModes extends Component {
    constructor() {
        super();

        this.state = ({showAlert: false, title: '', inEnglish: false});
        this.changeTextToEnglish = this.changeTextToEnglish.bind(this);
        this.changeTextToSpanish = this.changeTextToSpanish.bind(this);
        window.changeTextToEnglish = this.changeTextToEnglish;
        window.changeTextToSpanish = this.changeTextToSpanish;

    }

    componentDidUpdate(nextProps, nextState) {

        if (!nextState.showAlert) {
            this.turnOffAlert = setTimeout(() => {
                this.setState(() => ({showAlert: false}))
            }, 3000);
        }
    }

    componentWillUnmount() {
        clearTimeout(this.turnOffAlert);
    }

    changeTextToEnglish() {
        this.setState(() => ({inEnglish: true}));
        window.changeSubatlasTextToEnglish();
        window.changeHeaderToEnglish();
        window.changeCreditsToEnglish();
    }


    changeTextToSpanish() {
        this.setState(() => ({inEnglish: false}));
        window.changeSubatlasTextToSpanish();
        window.changeHeaderToSpanish();
        window.changeCreditsToSpanish();
    }

    render() {
        let english = this.state.inEnglish;
        return (

            <div>
                {english || localStorage.getItem(LANGUAGE_MODE) === ENGLISH ? (
                        <div title="There are two execution modes:
                        (a) Study: structures are colored individually.
                        (b) Review: structures are kept colored.">
                            <h6>Execution mode:</h6>
                            <div className="row">
                                <div className="col-6">
                                    <ButtonGroup>
                                        <Button className='ml-3 mr-3 btn-sm' color="primary" onClick={() => {
                                            localStorage.setItem(STUDY_MODE, STUDY);
                                            this.setState({showAlert: true, title: `Study mode`})
                                        }}
                                        >
                                            Study
                                        </Button>
                                        <Button className='ml-3 mr-3 btn-sm' color="primary" onClick={() => {
                                            localStorage.setItem(STUDY_MODE, REVIEW);
                                            this.setState({showAlert: true, title: `Review mode`})
                                        }}>Review</Button>
                                    </ButtonGroup>
                                </div>
                            </div>
                        </div>
                    ) :
                    (
                        <div title="Existen dos modalidades
                        de ejecución:
                        (a) Estudio: las estructuras se analizan individualmente.
                        (b) Repaso: las estructuras se analizan integralmente.">
                            <h6>Modo de ejecución:</h6>
                            <div className="row">
                                <div className="col-6">
                                    <ButtonGroup>
                                        <Button className='ml-3 mr-3 btn-sm' color="primary" onClick={() => {
                                            localStorage.setItem(STUDY_MODE, STUDY);
                                            this.setState({showAlert: true, title: `Modo estudio`})
                                        }}
                                        >
                                            Estudio
                                        </Button>
                                        <Button className='ml-3 mr-3 btn-sm' color="primary" onClick={() => {
                                            localStorage.setItem(STUDY_MODE, REVIEW);
                                            this.setState({showAlert: true, title: `Modo repaso`})
                                        }}>Repaso</Button>
                                    </ButtonGroup>
                                </div>
                            </div>
                        </div>
                    )
                }
                <div className="row">
                    <div className="col-12">
                        {
                            this.state.showAlert &&
                            <Alert
                                type='success'
                                show={this.state.showAlert}
                                title={this.state.title}
                            />
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default StudyModes;
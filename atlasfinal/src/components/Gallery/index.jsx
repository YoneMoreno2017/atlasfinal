/*Gallery shows each Parte's miniatura (PNG) as a link and an image*/
import React, {Component} from 'react';
import './styles.css';
import {getParameterByName} from "../../constantRoutes";
import Link from "react-router-dom/es/Link";


export default class Gallery extends Component {

    onClickFunction = (index) => {
        window.partIndex = index;
        window.rebuildNrrdCanvas();
        window.rebuildSegmentsList();
    };

    render() {
        return (
            this.props.IndexAtlas.getSubAtlasActual().getSubAtlas().map((parte, index) => {
                return (
                    <Link
                        to={`?parte=${index}&subAtlas=${getParameterByName('subAtlas')}`}
                        onClick={() => this.onClickFunction(index)}
                        key={index}
                    >
                        <img
                            title={parte.title}
                            className='miniature'
                            src={parte.getMiniatura()}
                        />
                    </Link>
                );
            })
        );
    }
};



/*Header has the responsability to let us access all subAtlas*/

import React, {Component} from 'react';
import Link from "react-router-dom/es/Link";
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';
import {
    BACKBONE_PAGE,
    HEAD_PAGE,
    COVER_PAGE,
    PELVIS_PAGE,
    RIB_PAGE,
    SCENE_PAGE,
    TORAX_PAGE, LANGUAGE_MODE, ENGLISH
} from '../../constantRoutes';
import './styles.css';

export default class Header extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
            inEnglish: false
        };

        this.changeHeaderToEnglish = this.changeHeaderToEnglish.bind(this);
        this.changeHeaderToSpanish = this.changeHeaderToSpanish.bind(this);
        window.changeHeaderToEnglish = this.changeHeaderToEnglish;
        window.changeHeaderToSpanish = this.changeHeaderToSpanish;
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    changeHeaderToEnglish() {
        this.setState(() => ({inEnglish: true}))
    }

    changeHeaderToSpanish() {
        this.setState(() => ({inEnglish: false}))
    }

    render() {
        let english = this.state.inEnglish;
        return (
            <div>
                {english || localStorage.getItem(LANGUAGE_MODE) === ENGLISH ? (
                        <div>
                            <Navbar color="light" light expand="md">
                                <NavbarBrand
                                    href={COVER_PAGE}>Horse's osteology
                                </NavbarBrand>
                            </Navbar>
                        </div>
                    )
                    :
                    (
                        <div>
                            <Navbar color="light" light expand="md">
                                <NavbarBrand
                                    href={COVER_PAGE}>Osteología del caballo
                                </NavbarBrand>
                            </Navbar>
                        </div>
                    )
                }
            </div>

        );
    }
}
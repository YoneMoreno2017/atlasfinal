import React from 'react';

const Alert = ({type, title, children, show, toggleShow}) => {
    return (
        <div>
            {
                show &&
                <div onClick={toggleShow} className={`alert alert-${type}`} role="alert">
                    {title ? title : children} </div>
            }
        </div>
    );
};

Alert.defaultProps = {
    show: true
};

export default Alert;
/*App shows the contents when the atlas has been loaded*/

import React, {Component} from 'react';
import BrowserRouter from "react-router-dom/es/BrowserRouter";
import Switch from "react-router-dom/es/Switch";
import Route from "react-router-dom/es/Route";
import Redirect from "react-router-dom/es/Redirect";
import ScenePage from "../ScenePage/index";
import CoverPage from "../CoverPage/index";
import {ATLAS_LOCATION, CATEDRA_MEDICA_PAGE, COVER_PAGE, CREDITS_PAGE, SCENE_PAGE} from "../../constantRoutes";
import JSONHeader from "../../model/JSONHeader";
import spinner from './spinner.gif';
import CreditsPage from "../CreditsPage/index";
import CatedraMedicaPage from "../CatedraMedicaPage/index";

export default class App extends Component {
    constructor(props) {
        super(props);

        const finishCallback = () => {
            this.setState({isAtlasLoading: false});
        };


        new JSONHeader(ATLAS_LOCATION, finishCallback);

        this.state = {
            isAtlasLoading: true
        };
    }

    render() {
        if (this.state.isAtlasLoading) {
            return (
                <img
                    className='spinner' //Required: we use it to removeSpinners() in SceneSubject
                    src={spinner}
                />
            );
        }
        return (
            <BrowserRouter>
                <div>
                    <Switch>
                        <Route exact path={COVER_PAGE} component={() => <CoverPage IndexAtlas={window.IndexAtlas}/>}/>
                        <Route path={SCENE_PAGE} component={() => <ScenePage IndexAtlas={window.IndexAtlas}/>}/>
                        <Route path={CREDITS_PAGE} component={() => <CreditsPage/>}/>
                        <Route path={CATEDRA_MEDICA_PAGE} component={() => <CatedraMedicaPage/>}/>
                        <Redirect from="*" to={COVER_PAGE}/>
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}
/*ScenePage is responsible for show the two canvas where we can interact*/
import React, {Component} from 'react';
import SplitterLayout from 'react-splitter-layout';
import './styles.css';
import Gallery from "../Gallery/index";
import SegmentsList from "../SegmentsList/index";
import CanvasAndTitle from "../CanvasAndTitle/index";
import Header from "../Header/index";


class ScenePage extends Component {

    render() {
        return (
            <div>
                <Header/>
                <SplitterLayout
                    vertical={false}
                    percentage={true}
                    primaryMinSize={20}
                    // secondaryMinSize={80}
                    secondaryInitialSize={80}
                >
                    <SplitterLayout
                        vertical={true}
                        percentage={true}
                        secondaryInitialSize={60}
                    >
                        <Gallery IndexAtlas={this.props.IndexAtlas}/>
                        <SegmentsList IndexAtlas={this.props.IndexAtlas}/>
                    </SplitterLayout>
                    <CanvasAndTitle IndexAtlas={this.props.IndexAtlas}/>
                </SplitterLayout>
            </div>
        );
    }
}

export default ScenePage;
/*Shows the app in the web*/
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles.css';
import App from "./components/App/index";



ReactDOM.render(
<App/>, document.getElementById('root'));

registerServiceWorker();



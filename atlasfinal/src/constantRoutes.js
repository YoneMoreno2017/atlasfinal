/*Declares all the app's routes*/

export const COVER_PAGE = '/';
export const SCENE_PAGE = '/scenePage';
export const HEAD_PAGE = '/scenePage?subAtlas=0';
export const BACKBONE_PAGE = '/scenePage?subAtlas=1';
export const RIB_PAGE = '/scenePage?subAtlas=2';
export const PELVIS_PAGE = '/scenePage?subAtlas=4';
export const TORAX_PAGE = '/scenePage?subAtlas=5';
export const CREDITS_PAGE = '/CreditsPage';
export const CATEDRA_MEDICA_PAGE = '/CatedraMedicaPage';

export function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

export const LANGUAGE_MODE = 'language';
export const SPANISH = 'etiquetas';
export const ENGLISH = 'etiquetasEN';


export const STUDY_MODE = 'mode';
export const STUDY = 'estudio';
export const REVIEW = 'repaso';

export const CLICKED_SEGMENT_ALPHA = '255';

export const ATLAS_LOCATION = '/atlas/json/';

